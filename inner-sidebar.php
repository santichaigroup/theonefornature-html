
<div class="row">
  <div class="col-12">
    <h3 class="gray text-center text-uppercase">RECOMMENDED FOR YOU</h3>
    <div class="row py-3">
      <?php if($i != 1) { ?>
      <div class="col-6 col-lg-3 mb-3">
        <a href="inner-1.php">
          <img src="./assets/img/ex_01.png" alt="B" class="d-block mx-auto mb-3">
          <h6 class="text-center gray">
            Tongtomyai homestay
          </h6>
        </a>
      </div>
      <?php }  if($i != 2) { ?>
      <div class="col-6 col-lg-3 mb-3">
        <a href="inner-2.php">
          <img src="./assets/img/ex_02.png" alt="B" class="d-block mx-auto mb-3">
          <h6 class="text-center gray">
            Trash Hero Phi Phi
          </h6>
        </a>
      </div>
      <?php }  if($i != 3) { ?>
      <div class="col-6 col-lg-3 mb-3">
        <a href="inner-3.php">
          <img src="./assets/img/ex_03.png" alt="B" class="d-block mx-auto mb-3">
          <h6 class="text-center gray">
            Soi Dog Foundation
          </h6>
        </a>
      </div>
      <?php }  if($i != 4) { ?>
      <div class="col-6 col-lg-3 mb-3">
        <a href="inner-4.php">
          <img src="./assets/img/ex_04.png" alt="B" class="d-block mx-auto mb-3">
          <h6 class="text-center gray">
            Elephant Nature Park 
          </h6>
        </a>
      </div>
      <?php }  if($i != 5) { ?>
      <div class="col-6 col-lg-3 mb-3">
        <a href="inner-5.php">
          <img src="./assets/img/ex_05.png" alt="B" class="d-block mx-auto mb-3">
          <h6 class="text-center gray">
            Ban Nai Soi Learning Center
          </h6>
        </a>
      </div>
      <?php }  if($i != 6) { ?>
      <div class="col-6 col-lg-3 mb-3">
        <a href="inner-6.php">
          <img src="./assets/img/ex_06.png" alt="B" class="d-block mx-auto mb-3">
          <h6 class="text-center gray">
            Diving & Marine <br> Conservation in Thailand
          </h6>
        </a>
      </div>
      <?php }  if($i != 7) { ?>
      <div class="col-6 col-lg-3 mb-3">
        <a href="inner-7.php">
          <img src="./assets/img/ex_07.png" alt="B" class="d-block mx-auto mb-3">
          <h6 class="text-center gray">
            Noistar Thai Animal <br> Rescue Foundation
          </h6>
        </a>
      </div>
      <?php }  if($i != 8) { ?>
      <div class="col-6 col-lg-3 mb-3">
        <a href="inner-8.php">
          <img src="./assets/img/ex_08.png" alt="B" class="d-block mx-auto mb-3">
          <h6 class="text-center gray">
          Volunteer Project Caring <br> for Elephants in Surin!
          </h6>
        </a>
      </div>
      <?php } ?>
    </div>
    <div class="row">
      <div class="col-12 text-center pb-5">
        <a href="inner-2.php" class="btn btn-outline-dark" style="min-width: 200px;">MORE</a>
      </div>
    </div>
  </div>
</div>
</div>