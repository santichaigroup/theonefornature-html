<?php include 'header.php'; ?>
  <section>
    <?php include 'nav.php'; ?>
  </section>

  <div class="nt__inner">
    <div class="nt__inner-bg" style="background-image: url(./assets/img/in-bg-9.png);">
      <h1 class="text-white">
        CHIANG RAI
      </h1>
      <h4 class="font-weight-lighter text-white">13-16 FEBUARY 2019</h4>
    </div>

    <div class="container mt-small">
      <div class="row">
        <div class="col-lg-5">
          <img src="./assets/img/i-9-1.png" alt="Inner" class="mb-3">
        </div>
        <div class="col-lg-7">
          <h2 class="text-uppercase gray"><img src="./assets/img/true.png" alt="T" style="vertical-align: baseline;"> Building greener and safer home</h2>
          <a href="#" class="badge badge-primary p-2">Share</a>
          <p class="my-3 font-weight-ligther" style="font-size: 1.5rem;">Be the one…to help prevent forest fires. Bring back rich natural resources and provide better home for all animals. Join our team to build firebreaks and to set up artificial mineral licks (salt lick) for animals in the area. Together, we can keep our forest beautiful.</p> 
          <ul class="p-0 m-0 my-3" style="font-size: 1rem;list-style: none;">
            <li>• Project Owner : Mon Volunteers</li>
            <li>• Date : 13 - 16 February 2020</li>
            <li>• Location : Chiang Rai</li>
          </ul>
          <p class="mb-0 font-weight-bold">Further information:</p>
          <p class="mb-0"><a href="https://www.facebook.com/MagicVolunteer/">https://www.facebook.com/MagicVolunteer/</a></p>
          <p class="mb-0">Call : +6665 391 9262 (NAE)</p>
          <p class="mb-0">Line : <a href="https://line.me/R/ti/p/%40magicvolunteer">https://line.me/R/ti/p/%40magicvolunteer</a></p>
        </div>
      </div>
      <div class="gallery h my-3">
        <figure class="gallery__item gallery__item--1">
          <img src="./assets/img/i-9-2.png" class="gallery__img" alt="Image 1">
        </figure>
        <figure class="gallery__item gallery__item--2">
          <img src="./assets/img/i-9-3.png" class="gallery__img" alt="Image 2">
        </figure>
        <figure class="gallery__item gallery__item--3">
          <img src="./assets/img/i-9-4.png" class="gallery__img" alt="Image 2">
        </figure>
      </div>
      <div class="google-maps">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d303780.28405069106!2d100.49000783766152!3d13.612406933369396!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311d6032280d61f3%3A0x10100b25de24820!2z4LiB4Lij4Li44LiH4LmA4LiX4Lie4Lih4Lir4Liy4LiZ4LiE4Lij!5e0!3m2!1sth!2sth!4v1574347867769!5m2!1sth!2sth" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
      </div>
      
      <?php $i = 9; ?>
      <?php include 'inner-sidebar.php'; ?>
    </div>
  </div>


<?php include 'footer.php'; ?>