<?php include 'header.php'; ?>
  <section>
    <?php include 'nav.php'; ?>
  </section>

  <div class="nt__commitment d-flex justify-content-center align-items-center">
    <div class="container">
      <div class="modal fade" id="newModalMobile" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog px-3" role="document">
          <img src="./assets/img/com-mobile.png" alt="C" class="img-fluid d-block mx-auto">
          <img src="./assets/img/reward-2.png" alt="rewa" class="img-fluid d-block mx-auto mt-3 mb-5">
          <div class="row d-flex justify-content-center align-items-center mb-3 d-md-none">
            <div class="col-12 text-center">
              <h5 class="font-weight-bold">“And The winner who get</h5>
              <h5 class="font-weight-bold">free trip for 2 to Thailand is”</h5>
            </div>
            <div class="col-12 my-2">
              <img src="./assets/img/winner.png" alt="Winner" class="img-fluid d-block mx-auto">
            </div>
            <div class="col-12 text-center">
              <h5 class="font-weight-bold">Yor Gis</h5>
              <h5 class="font-weight-bold">Greece</h5>
            </div>
          </div>
          <div class="row justify-content-center align-items-center mb-5 d-none d-md-flex">
            <div class="col-md-4 text-right">
              <h6 class="font-weight-bold" style="font-size: .75rem">“And The winner who get</h6>
              <h6 class="font-weight-bold" style="font-size: .75rem">free trip for 2 to Thailand is”</h6>
            </div>
            <div class="col-md-3">
              <img src="./assets/img/winner.png" alt="Winner" class="img-fluid d-block mx-auto">
            </div>
            <div class="col-md-4">
              <h6 class="font-weight-bold">Yor Gis</h6>
              <h6 class="font-weight-bold">Greece</h6>
            </div>
          </div>
          <p class="mb-0 font-weight-bold text-uppercase">Follow this step to get your prize</p>
          <ol class="pl-3">
            <li>Please verify your identity using passport and email us at <a href="mailto:contact@theonefornature.com">contact@theonefornature.com</a>  within 3 months after  
              the announcement. </li>
            <li> Tell us your choice of responsible tourism from provided option </li>
          </ol>

          <p class="pl-3">There you go! Pack your bag and get ready to Travel with responsibility in Thailand </p>
          
          <div class="text-danger mb-3">
            <h5 class="text-uppercase mb-0">remark:</h5> 
            <p class="mb-0">*The Tourism Authority of Thailand reserves the right to disqualify the winner if he/she doesn’t response within 2 weeks after 
              theonefornature team reaching out email.</p>
            <p class="mb-0">*The winner must declare the copy of passport and receive the prize within 3 months after winner announcement.</p>
          </div>
          <div class="text-center">
            <button type="button" class="btn btn-primary w-100" data-dismiss="modal">CLOSE</button>
          </div>
        </div>
      </div>
      <div class="modal fade" id="newModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
          <div class="modal-content modal-com">
            <div class="px-5">
              <img src="./assets/img/modal-head.png" alt="Modal Head" class="mb-3 img-fluid d-block mx-auto">
            </div>
            <section>

            <div class="row d-flex justify-content-center align-items-center mb-3">
              <div class="col-lg-4 text-right">
                <h6 class="font-weight-bold down-size">“And The winner who get</h6>
                <h6 class="font-weight-bold down-size">free trip for 2 to Thailand is”</h6>
              </div>
              <div class="col-lg-2">
                <img src="./assets/img/winner.png" alt="Winner" class="img-fluid d-block mx-auto">
              </div>
              <div class="col-lg-4">
                <h6 class="font-weight-bold down-size">Yor Gis</h6>
                <h6 class="font-weight-bold down-size">Greece</h6>
              </div>
            </div>

            <p class="mb-0 font-weight-bold text-uppercase">Follow this step to get your prize</p>
            <ol class="pl-3" style="font-size:.75rem">
              <li>Please verify your identity using passport and email us at <a href="mailto:contact@theonefornature.com">contact@theonefornature.com</a>  within 3 months after  
                the announcement. </li>
              <li> Tell us your choice of responsible tourism from provided option </li>
            </ol>

            <p class="pl-3" style="font-size:.75rem">There you go! Pack your bag and get ready to Travel with responsibility in Thailand </p>
            
            <div class="text-danger mb-3">
              <h6 class="text-uppercase mb-0">remark:</h6> 
              <p class="mb-0" style="font-size:.75rem">*The Tourism Authority of Thailand reserves the right to disqualify the winner if he/she doesn’t response within 2 weeks after 
                theonefornature team reaching out email.</p>
              <p class="mb-0" style="font-size:.75rem">*The winner must declare the copy of passport and receive the prize within 3 months after winner announcement.</p>
            </div>
            </section>
            <div class="text-center">
              <button type="button" class="btn btn-primary" data-dismiss="modal">CLOSE</button>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xl-6 position-relative pb-5 px-sm-5 px-xl-0">
          <h2 class="text-center text-xl-left mobile-title mb-3 mb-xl-0" style="position: relative;z-index: 999;">  
            <span class="d-xl-none" style="font-size: 2.5rem;">Your commitment <br class="d-none d-xl-inline"></span>
            <span class="d-none d-xl-inline">Your commitment <br class="d-none d-xl-inline"></span>
            <br class="d-xl-none"> can change <br class="d-none d-xl-inline">
            the world. <br class="d-none d-xl-inline">
          </h2>
          <img src="./assets/img/reward-2.png" alt="Reward-2" class="nt__reward2">
          <h5 class="d-xl-none tr-4 my-3">Share your commitment for a chance to win a <span class="tr-5 font-weight-bold">FREE Trip</span> For 2 to Thailand worth USD 3,000 Winner will be Announced on 9 Jan 2020</h5>
          <img src="./assets/img/reward2.png" alt="Commitment" class="d-xl-none d-block mx-auto">
          <h5 class="mt-5 mb-3 mb-xl-3 d-none d-xl-block">Share your commitment for a chance to win a <span class="display-4 font-weight-bold">FREE Trip</span> For 2 to Thailand worth USD 3,000 Winner will be Announced on 9 Jan 2020</h5>
          <div class="d-xl-none text-center mb-5 orde">
            <a href="commitment2.php" class="btn btn-primary btn-lg mt-4 bcom"><span>Commitment</span></a>
          </div>
          <h4 class="h4 font-weight-bold">DETAIL REWARD</h4>
          <div class="divscroll">
            <h5 class="font-weight-bold">The winner can choose their ways of travel with responsibility between 1. or 2. </h5>
            <ol>
              <li>Volunteering at journey to Freedom Volunteer in Chiangmai</li>
              <li>Scuba Diving Trip in Trang province</li>
            </ol>

            <p>1. Volunteering at Journey to Freedom Volunteer is a unique project which features daily elephant interactions, learning about agriculture and community, developing local school children learning experiences and living in a cultural exchange at a tribal village in the mountains of Chiang Mai.</p>

            <p>Be part of a conservation team that brings joy to lives of this community and assists in the daily care of elephants for one week.</p>

            <h5 class="font-weight-bold">Highlights</h5>
            <ul>
              <li>Daily time with elephants; feed and walk alongside these gentle giants through the jungle.</li>
              <li>Gain a fascinating insight to the life of elephants in their natural habitat and observe the way they behave when surrounded by the jungle.</li>
              <li>Delicious Vegetarian Thai food, freshly cooked each day for you to enjoy.</li>
              <li>Meet people from all walks of life with similar interests and make life long friends.</li>
              <li>Make a difference in a community while learning about their traditional way of life</li>
              <li>Be apart of a blessing ceremony which thanks you for your contribution to the community</li>
            </ul>

            <p>2. Scuba diving trip in Trang province, you can choose between 2.1 or 2.2</p>
              <p class="pl-5">2.1 Open water course (no PADI license only)</p>
            <p>For those who never experience diving before, get a chance to learn diving lesson with Local PADI Dive Instructor at Laytrang Diving and gain open water certificate.</p>
            <h5 class="font-weight-bold"> Highlight itinerary </h5>
            <p>Day 1 Learn diving theory for 5 lesson </p>
            <p>Day 2 Learn practical dive in swimming pool</p>
            <p>Day 3 Test in the sea for 2 dive (Trang province)</p>
            <p>Day 4 Test in the sea for 2 dive (Trang province)</p>

              <p class="pl-5">2.2 Scuba diving trip (Only for certificate diver)</p>
            <p>For the one who have diving license, you can experience a special dive in one of Thailand best spot</p>
            <h5 class="font-weight-bold">Highlight itinerary </h5>
            <p>DAY 1 Explore the unseen Le khao Kop cave in Trang province</p>
            <p>DAY 2 Diving at Koh Rok for 2 dive </p>
            <p>Day 3 Diving at Koh Laoliang for 2 dive and Koh Libong for 1 dive</p>
            <p>DAY 4 Watch Dugong at Koh Libong </p>


            <h5 class="font-weight-bold">Rules</h5>
            <ol>
              <li>Register at www.TheOneforNature.com</li>   
              <li>Choose the picture of nature that suite you.</li> 
              <li>Choose your commitment to the nature.</li> 
              <li>Share your thought to save nature with #TheOneForNature and #AmazingThailand onFacebook.</li> 
              <li>Set Facebook post as public.</li> 
              <li>The winner will receive round trip economy flight ticket from your country of resident to Bangkok with accommodation and Responsible Tourism package tour in Thailand for 9 days and 8 nights for 2 people.</li> 
            </ol>

            <h5 class="font-weight-bold">Qualification</h5>
            <ul style="list-style:none;padding:0;">
              <li>- Must be 18 years of age.</li>
              <li>- Must be residence in America and Europe continents.</li>
            </ul>

            <h5 class="font-weight-bold">Selection Criteria </h5>
            <p>Randomly Selection.</p>

            <h5 class="font-weight-bold">Competition Period</h5>
            <p>Start from 18 October 2019 until 6 January 2019 at 24:00 (Thailand local time)</p>
            
            <h5 class="font-weight-bold">Winner Announcement</h5>
            <p>9 December 2019 at 18:00 (Thailand local time)</p>

            <h5 class="font-weight-bold">Term & Condition </h5>
            <ol>
              <li>The prize cannot exchange into money or transfer to another person.
              <li>Tourism Authority of Thailand reserves the right to disqualify for profanity, indecent, illegal, or vuxlar both visual and message without advance notice.</li>
              <li>The winner must have a passport with minimum 6 months before expiration to be eligible for the prize.</li>
              <li>The winner must declare the copy of passport and receive the prize within 3 months after winner announcement.</li>
              <li>The prize is not included Visa Fee and insurance. The winner and companion are obligation to those cost.</li>
              <li>The winner is solely obligated to withholding tax or any other (if applied) occur from the prize</li>
              <li>The winner must obey to term & condition.  If there are corruption, illegal, or not following the term & condition, Tourism Authority of Thailand reserves the right to disqualify the participant without advance notice.</li>
              <li>Tourism Authority of Thailand has the right to change the prize of add the term and condition without advance notice.  Prize can’t exchange to money or any other stuff. </li>
              <li>If Tourism Authority of Thailand found out that any participant is not following term & condition or sabotage the activity.  Tourism Authority of Thailand has solely right to disqualify that person.</li>
              <li>Participant has no right to ask for compensation or other benefit both criminal and civil laws.</li>
              <li>Tourism Authority of Thailand reserve the right to publish information regarding activity for marketing and advertising for reasonable purpose.  Tourism Authority of Thailand also has the right to publish name and surname of the winner both in the present and in the future, which Tourism Authority of Thailand considered to be granted permission by the participant.</li>
              <li>Tourism Authority of Thailand will not take responsibility on lost property or theft in any case.</li>
              <li>Tourism Authority of Thailand decision is final in any circumstance.</li>
              <li>For more information, please contact at contact@theonefornature.com</li>
            </ol>
          </div>
          <div class="text-center">
            <a href="commitment2.php" class="btn btn-primary btn-lg bcom mt-4"><span>Commitment</span></a>
          </div>
        </div>
        <div class="col-xl-6 d-flex align-items-center">
          <img src="./assets/img/reward2.png" alt="Commitment" class="d-none d-xl-block mx-auto">
        </div>
      </div>
    </div>
  </div>

<?php include 'footer.php'; ?>

<script>
  
    $(window).on('load',function(){
      if($(window).width() > 1279)
      {
        $('#newModal').modal('show');
      }
      if($(window).width() < 1280) {
        $('#newModalMobile').modal('show');
      }
    });
</script>