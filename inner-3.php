<?php include 'header.php'; ?>
  <section>
    <?php include 'nav.php'; ?>
  </section>

  <div class="nt__inner">
    <div class="nt__inner-bg" style="background-image: url(./assets/img/in-bg-3.png);">
      <h1 class="text-white">
        Phuket
      </h1>
    </div>

    <div class="container mt-small">
      <div class="row">
        <div class="col-lg-5">
          <img src="./assets/img/ex_03.png" alt="Inner" class="mb-3">
        </div>
        <div class="col-lg-7">
          <h2 class="text-uppercase gray"><img src="./assets/img/true.png" alt="T" style="vertical-align: baseline;"> Soi Dog Foundation</h2>
          <a href="#" class="badge badge-primary p-2">Share</a>
          <p class="my-3 font-weight-ligther" style="font-size: 1.5rem;">Soi Dog Foundation is a non-profit that aims to help Thailand’s street animals through adoption programs, vaccination and sterilizations.</p> 
          <p class="mb-3"><b>Your role: be the one…who</b> can help street animal at their shelter in Phuket for a month or longer, and also volunteers who can help fly animals to their new adopted homes.</p>
          <p class="mb-0"><b>Location:</b> Phuket</p>
          <p class="mb-0">If you’re interested in being a flight volunteer, email <a href="mailto:flightvolunteer@soidog.org">flightvolunteer@soidog.org</a>.</p>
          <p class="mb-0">If you’d like to volunteer at the shelter, email <a href="mailto:volunteering@soidog.org">volunteering@soidog.org</a>.</p>
          <p class="mb-0">Learn more: <a href="https://www.soidog.org/">https://www.soidog.org/</a></p>
          
        </div>
      </div>
      <div class="gallery my-3">
        <figure class="gallery__item t2 gallery__item--1">
          <img src="./assets/img/3i1.jpg" class="gallery__img" alt="Image 1">
        </figure>
        <figure class="gallery__item t2 gallery__item--2">
          <img src="./assets/img/3i2.jpg" class="gallery__img" alt="Image 2">
        </figure>
        <figure class="gallery__item t2 gallery__item--3">
          <img src="./assets/img/3i3.jpg" class="gallery__img" alt="Image 2">
        </figure>
      </div>
      <div class="google-maps">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d303780.28405069106!2d100.49000783766152!3d13.612406933369396!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311d6032280d61f3%3A0x10100b25de24820!2z4LiB4Lij4Li44LiH4LmA4LiX4Lie4Lih4Lir4Liy4LiZ4LiE4Lij!5e0!3m2!1sth!2sth!4v1574347867769!5m2!1sth!2sth" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
      </div>
      
      <?php $i = 3; ?>
      <?php include 'inner-sidebar.php'; ?>
    </div>
  </div>

<?php include 'footer.php'; ?>