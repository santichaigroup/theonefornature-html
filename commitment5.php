<?php include 'header.php'; ?>
  <section>
    <?php include 'nav.php'; ?>
  </section>

  <div class="nt__commitment delete d-flex justify-content-center align-items-center flex-column">
    <div class="container">
      <div class="text-center text-size-responsive">
        <h1 class="ty">THANK YOU TO BE</h1>
      </div>
      <div class="row">
        <div class="col-12 py-5 px-5 px-lg-0 text-center">
          <img src="./assets/img/ty.png" alt="Thank">
        </div>
      </div>
      <div class="container py-5">
        <div class="d-flex justify-content-center">
          <a href="commitment4.php" class="btn btn-primary btn-lg bpn mx-3 mx-xl-5 px-xl-5">Back</a>
        </div>
      </div>
    </div>
  </div>

<?php include 'footer.php'; ?>