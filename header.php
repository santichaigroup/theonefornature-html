<?php
define( 'ROOT', 'http://localhost/theonefornature' );
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>The One for Nature</title>
  <link rel="icon" href="ICON32x32.png" type="image/x-icon" />
  <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="node_modules/@fortawesome/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="assets/css/fonts.css">
  <link rel="stylesheet" href="assets/css/bootstrap-edit.css">
  <link rel="stylesheet" href="assets/lightbox/css/lightbox.min.css">
  <link rel="stylesheet" href="style.css">
</head>
<body>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v5.0&appId=2686513391415357&autoLogAppEvents=1"></script>
  <?php $i = 0; ?>