<?php include 'header.php'; ?>
  <section class="nt__main--bg">
    <?php $i = 1;?>
    <?php include 'nav.php'; ?>
    <div class="wrapper">
      <div class="container">
        <div class="text-right text-white fr-1">
          PEOPLE WHO JOINED OUR CAUSE : 231
        </div>
        <div class="row py-5">
          <div class="col-md-5 col-xl-5 d-flex justify-content-center justify-content-xl-end align-items-center">
            <div class="box-image"></div>
          </div>
          <div class="d-lg-flex col-md-7 col-xl-7 justify-content-center justify-content-xl-start align-items-center">
            <img src="assets/img/reward.png" alt="Reward" style="max-height: 350px;" class="px-3 px-lg-0">
          </div>
          <div class="col-12 text-center text-white pt-3">
            <h4 class="font-weight-bold">Announcing The Winner of <br class="d-sm-none"><span class="nt__promotion">FREE TRIP</span> to Thailand <br class="d-sm-none"> on 9 JAN 2020 !!</h4>
            <h5 class="font-weight-bold promotioner">Click commitment button <br class="d-sm-none"> to see the result</h5>
            <a href="commitment1.php" class="btn btn-primary btn-lg mt-4"><span>Commitment</span></a>
          </div>
        </div>
      </div>
    </div>
    <div class="py-3 py-md-5">
      <div class="wrapper">
        <div class="container">
          <div class="row">
            <div class="col-lg-6">
              <a href="#" data-toggle="modal" data-target="#new">
                <img src="assets/img/new-video.png" alt="New Video">
              </a>
              <div class="modal fade" id="new" tabindex="-1" role="dialog" aria-labelledby="NewLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                    <iframe style="width:100%;height:300px" src="https://www.youtube.com/embed/Q0NPYpQmTTo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6 d-flex flex-column justify-content-center">
              <blockquote class="px-3 px-lg-0">
                <p>Because nature need us, It’s time <br> to become “The One for Nature” <br> and travel with responsibility <br> in Thailand.
                <br><b>Check out our achievement video!!</b></p>
              </blockquote>
              <a href="#vdo" class="nt__mored px-3"><span>More</span></a>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-md-10">
            </div>
          </div>
        </div>
      </div>
    </div>
    <section class="event-section" id="event">
      <div class="wrapper">
        <div class="container">
          <h2 class="nt__title--line">Event</h2>
          <a href="#">
            <figure class="pt-4 pb-3">
              <img src="./assets/img/kohtao.png" alt="Koh Tao">
            </figure>
          </a>
          <div class="row py-3">
            <div class="col-lg-5">
              <div class="container-fluid">
                <div class="row">
                  <div class="column-12 mb-3">
                    <a href="#"><img src="./assets/img/pic-1.png" alt="Picture 1" class="w-100"></a>
                  </div>
                  <div class="column-6 mb-3">
                    <a href="#"><img src="./assets/img/pic-4.jpg" alt="Picture 2" class="w-100"></a>
                  </div>
                  <div class="column-6 mb-3">
                    <a href="#"><img src="./assets/img/pic-5.jpg" alt="Picture 3" class="w-100"></a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-7">
              <div class="row rowspecial">
                <div class="col-12 mb-3 h-100">
                  <div class="nt__screen--white h-100 d-flex justify-content-center align-items-center">
                    <div>
                      <h3 class="font-weight-bold">NEW PARADISE BY YOU</h3>
                      <p class="font-weight-lighter"><em>Be the one…to bring underwater paradise back to life at Koh Tao Island, Surat Thani. Help building new ecosystem for aquatic animals, create new exotic diving spots for tourists by making special pots for new sprouting corals, and dive down to place them under the sea.</em></p>
                      <dl class="row">
                        <dt class="col-3">AGENDA:</dt>
                        <dd class="col-9">
                          <p class="mb-0 header-list">8 th - 9th January 2020</p>
                          <ul class="the-menu">
                            <li>Activity troop, Sairee Beach, Koh Tao. Surat Thani.</li>
                          </ul>
                          <p class="mb-0 header-list">11th January 2020</p>
                          <ul class="the-menu">
                            <li>Diving with dive schools from Koh Tao.</li>
                          </ul>
                        </dd>
                      </dl>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </section>

  <section class="nt__explore px-0 px-md-5 px-lg-0" id="explore">
    <div class="wrapper">
      <div class="container-fluid py-3">
        <h2 class="nt__title--line text-white">Explore</h2>
        <h3 class="text-center text-white">Explore volunteer activity <br class="d-none d-lg-inline"> for nature in Thailand</h3>
      </div>
    </div>
    <div class="wrapper">
      <div class="container-fluid">
        <div class="row desktop">
          <div class="col-6 col-lg-4 my-3">
            <div class="card d-flex h-100">
              <a href="inner-1.php" class="order-1">
                <img src="./assets/img/ex_01.png" class="card-img-top" alt="Explore">
              </a>
              <div class="card-body order-2">
                <h5 class="card-title font-weight-lighter text-center">Tongtomyai homestay</h5>
              </div>
              <div class="card-footer order-3">
                <a href="inner-1.php" class="nt__more"><span>Explore</span></a>
              </div>
            </div>
          </div>
          <div class="col-6 col-lg-4 my-3">
            <div class="card d-flex h-100">
              <a href="inner-2.php" class="order-3">
                <img src="./assets/img/ex_02.png" class="card-img-top" alt="Explore">
              </a>
              <div class="card-body order-1">
                <h5 class="card-title font-weight-lighter text-center">Trash Hero Phi Phi</h5>
              </div>
              <div class="card-footer order-2">
                <a href="inner-2.php" class="nt__more"><span>Explore</span></a>
              </div>
            </div>
          </div>
          <div class="col-6 col-lg-4 my-3">
            <div class="card d-flex h-100">
              <a href="inner-3.php" class="order-1">
                <img src="./assets/img/ex_03.png" class="card-img-top" alt="Explore">
              </a>
              <div class="card-body order-2">
                <h5 class="card-title font-weight-lighter text-center">Soi Dog Foundation</h5>
              </div>
              <div class="card-footer order-3">
                <a href="inner-3.php" class="nt__more"><span>Explore</span></a>
              </div>
            </div>
          </div>
        </div>
        <div class="row desktop">
          <div class="col-6 col-lg-4 my-3">
            <div class="card d-flex h-100">
              <a href="inner-4.php" class="order-1">
                <img src="./assets/img/ex_04.png" class="card-img-top" alt="Explore">
              </a>
              <div class="card-body order-2">
                <h5 class="card-title font-weight-lighter text-center">Elephant Nature Park</h5>
              </div>
              <div class="card-footer order-3">
                <a href="inner-4.php" class="nt__more"><span>Explore</span></a>
              </div>
            </div>
          </div>
          <div class="col-6 col-lg-4 my-3">
            <div class="card d-flex h-100">
              <a href="inner-5.php" class="order-3">
                <img src="./assets/img/ex_05.png" class="card-img-top" alt="Explore">
              </a>
              <div class="card-body order-1">
                <h5 class="card-title font-weight-lighter text-center">Ban Nai Soi Learning Center</h5>
              </div>
              <div class="card-footer order-2">
                <a href="inner-5.php" class="nt__more"><span>Explore</span></a>
              </div>
            </div>
          </div>
          <div class="col-6 col-lg-4 my-3">
            <div class="card d-flex h-100">
              <a href="inner-6.php" class="order-1">
                <img src="./assets/img/ex_06.png" class="card-img-top" alt="Explore">
              </a>
              <div class="card-body order-2">
                <h5 class="card-title font-weight-lighter text-center">Diving & Marine <br> Conservation in Thailand</h5>
              </div>
              <div class="card-footer order-3">
                <a href="inner-6.php" class="nt__more"><span>Explore</span></a>
              </div>
            </div>
          </div>
        </div>
        <div class="row desktop">
          <div class="col-6 col-lg-4 my-3">
            <div class="card d-flex h-100">
              <a href="inner-7.php" class="order-1">
                <img src="./assets/img/ex_07.png" class="card-img-top" alt="Explore">
              </a>
              <div class="card-body order-2">
                <h5 class="card-title font-weight-lighter text-center">Noistar Thai Animal <br> Rescue Foundation</h5>
              </div>
              <div class="card-footer order-3">
                <a href="inner-7.php" class="nt__more"><span>Explore</span></a>
              </div>
            </div>
          </div>
          <div class="col-6 col-lg-4 my-3">
            <div class="card d-flex h-100">
              <a href="inner-8.php" class="order-3">
                <img src="./assets/img/ex_08.png" class="card-img-top" alt="Explore">
              </a>
              <div class="card-body order-1">
                <h5 class="card-title font-weight-lighter text-center">Volunteer Project Caring <br> for Elephants in Surin!</h5>
              </div>
              <div class="card-footer order-2">
                <a href="inner-8.php" class="nt__more"><span>Explore</span></a>
              </div>
            </div>
          </div>
        </div>
  
        <div class="row mobile">
          <div class="col-6 col-lg-4 my-3">
            <div class="card d-flex h-100">
              <a href="inner-1.php" class="order-1">
                <img src="./assets/img/ex_01.png" class="card-img-top" alt="Explore">
              </a>
              <div class="card-body order-2">
                <h5 class="card-title font-weight-lighter text-center">Tongtomyai homestay</h5>
              </div>
              <div class="card-footer order-3">
                <a href="inner-1.php" class="nt__more"><span>Explore</span></a>
              </div>
            </div>
          </div>
          <div class="col-6 col-lg-4 my-3">
            <div class="card d-flex h-100">
              <a href="inner-2.php" class="order-3">
                <img src="./assets/img/ex_02.png" class="card-img-top" alt="Explore">
              </a>
              <div class="card-body order-1">
                <h5 class="card-title font-weight-lighter text-center">Trash Hero Phi Phi</h5>
              </div>
              <div class="card-footer order-2">
                <a href="inner-2.php" class="nt__more"><span>Explore</span></a>
              </div>
            </div>
          </div>
        </div>
        <div class="row mobile">
          <div class="col-6 col-lg-4 my-3">
            <div class="card d-flex h-100">
              <a href="inner-3.php" class="order-1">
                <img src="./assets/img/ex_03.png" class="card-img-top" alt="Explore">
              </a>
              <div class="card-body order-2">
                <h5 class="card-title font-weight-lighter">Soi Dog Foundation</h5>
              </div>
              <div class="card-footer order-3">
                <a href="inner-3.php" class="nt__more"><span>Explore</span></a>
              </div>
            </div>
          </div>
          <div class="col-6 col-lg-4 my-3">
            <div class="card d-flex h-100">
              <a href="inner-4.php" class="order-3">
                <img src="./assets/img/ex_04.png" class="card-img-top" alt="Explore">
              </a>
              <div class="card-body order-1">
                <h5 class="card-title font-weight-lighter">Elephant Nature Park </h5>
              </div>
              <div class="card-footer order-2">
                <a href="inner-4.php" class="nt__more"><span>Explore</span></a>
              </div>
            </div>
          </div>
        </div>
        <div class="row mobile">
          <div class="col-6 col-lg-4 my-3">
            <div class="card d-flex h-100">
              <a href="inner-5.php" class="order-1">
                <img src="./assets/img/ex_05.png" class="card-img-top" alt="Explore">
              </a>
              <div class="card-body order-2">
                <h5 class="card-title font-weight-lighter">Ban Nai Soi Learning Center</h5>
              </div>
              <div class="card-footer order-3">
                <a href="inner-5.php" class="nt__more"><span>Explore</span></a>
              </div>
            </div>
          </div>
          <div class="col-6 col-lg-4 my-3">
            <div class="card d-flex h-100">
              <a href="inner-6.php" class="order-3">
                <img src="./assets/img/ex_06.png" class="card-img-top" alt="Explore">
              </a>
              <div class="card-body order-1">
                <h5 class="card-title font-weight-lighter">Diving & Marine <br> Conservation in Thailand</h5>
              </div>
              <div class="card-footer order-2">
                <a href="inner-6.php" class="nt__more"><span>Explore</span></a>
              </div>
            </div>
          </div>
        </div>
        <div class="row mobile">
          <div class="col-6 col-lg-4 my-3">
            <div class="card d-flex h-100">
              <a href="inner-7.php" class="order-1">
                <img src="./assets/img/ex_07.png" class="card-img-top" alt="Explore">
              </a>
              <div class="card-body order-2">
                <h5 class="card-title font-weight-lighter">Noistar Thai Animal <br> Rescue Foundation</h5>
              </div>
              <div class="card-footer order-3">
                <a href="inner-7.php" class="nt__more"><span>Explore</span></a>
              </div>
            </div>
          </div>
          <div class="col-6 col-lg-4 my-3">
            <div class="card d-flex h-100">
              <a href="inner-8.php" class="order-3">
                <img src="./assets/img/ex_08.png" class="card-img-top" alt="Explore">
              </a>
              <div class="card-body order-1">
                <h5 class="card-title font-weight-lighter">Volunteer Project Caring <br> for Elephants in Surin!</h5>
              </div>
              <div class="card-footer order-2">
                <a href="inner-8.php" class="nt__more"><span>Explore</span></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="nt__vdo" id="vdo">
    <div class="wrapper">
      <div class="container">
        <h2 class="nt__title--line text-white vdo-line">VDO</h2>
        <blockquote>
          <h3>LITTLE DID WE KNOW WE ARE HARMING NATURE <br class="d-none d-lg-inline"> MORE THAN LOVING THEM</h3>
        </blockquote>
        <p class="text-white text-center">It’s time for us to become The One for Nature and travel <br class="d-none d-lg-inline">with responsibility in Thailand.</p>
  
        <div class="row justify-content-center py-3">
          <div class="col-md-10 col-lg-6 mb-3">
            <a href="#" data-toggle="modal" data-target="#rachel">
              <div class="nt__vdo--card" style="box-shadow: 0 1rem 1rem rgba(0,0,0,.375)!important;">
                <img src="./assets/img/video-1.png" alt="Video" class="w-100">
                <div class="overlay">
                  <div class="nt__vdo--title d-flex justify-content-center align-items-center">
                    <span class="text-uppercase">Rachel Rudwell</span> 
                    <i class="far fa-play-circle ml-2" style="font-size: 2rem;"></i>
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="modal fade" id="rachel" tabindex="-1" role="dialog" aria-labelledby="RachelLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="RachelLabel">Rachel Rudwell</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <iframe style="width:100%;height:300px" src="https://www.youtube.com/embed/bqjmUwF4wRI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-10 col-lg-6 mb-3">
            <a href="#" data-toggle="modal" data-target="#martin">
              <div class="nt__vdo--card" style="box-shadow: 0 1rem 1rem rgba(0,0,0,.375)!important;">
                <img src="./assets/img/video-2.png" alt="Video" class="w-100">
                <div class="overlay">
                  <div class="nt__vdo--title d-flex justify-content-center align-items-center">
                    <span class="text-uppercase">Martin Solhaugen</span> 
                    <i class="far fa-play-circle ml-2" style="font-size: 2rem;"></i>
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="modal fade" id="martin" tabindex="-1" role="dialog" aria-labelledby="MartinLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="MartinLabel">Martin Solhaugen</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                <iframe style="width:100%;height:300px" src="https://www.youtube.com/embed/T54TPJ0jl3c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 text-center">
            <a href="commitment1.php" class="btn btn-primary btn-lg mt-4"><span>Commitment</span></a>
          </div>
        </div>
      </div>
      <div class="container mt-5">
        <h2 class="nt__title--line">Gallery</h2>
        <div id="top_inner" class="row py-3">
          <div class="col-12">
            <div class="row px-md-5">
            <div class="col-6 col-lg-4 mb-3">
            <a href="assets/img/g1.jpeg" data-lightbox="roadtrip">
              <img src="assets/img/g1.jpeg" alt="" class="w-100">
            </a>
          </div>
          <div class="col-6 col-lg-4 mb-3">
            <a href="assets/img/g2.jpeg" data-lightbox="roadtrip">
              <img src="assets/img/g2.jpeg" alt="" class="w-100">
            </a>
          </div>
          <div class="col-6 col-lg-4 mb-3">
            <a href="assets/img/g3.jpeg" data-lightbox="roadtrip">
              <img src="assets/img/g3.jpeg" alt="" class="w-100">
            </a>
          </div>
          <div class="col-6 col-lg-4 mb-3">
            <a href="assets/img/g4.jpeg" data-lightbox="roadtrip">
              <img src="assets/img/g4.jpeg" alt="" class="w-100">
            </a>
          </div>
          <div class="col-6 col-lg-4 mb-3">
            <a href="assets/img/g5.jpeg" data-lightbox="roadtrip">
              <img src="assets/img/g5.jpeg" alt="" class="w-100">
            </a>
          </div>
          <div class="col-6 col-lg-4 mb-3">
            <a href="assets/img/g6.jpeg" data-lightbox="roadtrip">
              <img src="assets/img/g6.jpeg" alt="" class="w-100">
            </a>
          </div>
          <div class="col-6 col-lg-4 mb-3 d-none d-lg-block">
            <a href="assets/img/g7.jpeg" data-lightbox="roadtrip">
              <img src="assets/img/g7.jpeg" alt="" class="w-100">
            </a>
          </div>
          <div class="col-6 col-lg-4 mb-3 d-none d-lg-block">
            <a href="assets/img/g8.jpeg" data-lightbox="roadtrip">
              <img src="assets/img/g8.jpeg" alt="" class="w-100">
            </a>
          </div>
          <div class="col-6 col-lg-4 mb-3 d-none d-lg-block">
            <a href="assets/img/g9.jpeg" data-lightbox="roadtrip">
              <img src="assets/img/g9.jpeg" alt="" class="w-100">
            </a>
          </div>
            </div>
          </div>
          <div  class="col-12 text-center">
            <button type="button" id="buttonshow" class="btn btn-primary btn-lg mt-4" style="min-width:270px"><span>SEE ALL</span></button>
          </div>
          <div id="dontshow" class="col-12 d-none">
            <div class="row px-md-5">
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g10.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g10.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g11.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g11.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g12.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g12.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g13.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g13.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g14.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g14.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g15.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g15.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g16.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g16.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g17.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g17.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g18.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g18.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g19.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g19.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g20.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g20.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g21.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g21.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g22.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g22.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g23.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g23.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g24.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g24.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g25.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g25.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g26.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g26.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g27.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g27.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g28.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g28.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g29.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g29.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g30.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g30.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g31.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g31.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g32.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g32.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g33.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g33.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g34.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g34.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g35.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g35.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g36.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g36.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g37.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g37.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g38.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g38.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/img/g39.jpeg" data-lightbox="roadtrip">
                  <img src="assets/img/g39.jpeg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t1.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t1.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t2.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t2.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t3.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t3.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t4.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t4.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t5.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t5.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t6.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t6.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t7.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t7.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t8.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t8.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t9.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t9.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t10.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t10.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t11.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t11.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t12.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t12.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t13.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t13.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t14.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t14.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t15.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t15.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t16.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t16.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t17.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t17.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t18.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t18.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t19.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t19.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t20.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t20.jpg" alt="" class="w-100">
                </a>
              </div>
              
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t21.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t21.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t22.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t22.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t23.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t23.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t24.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t24.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t25.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t25.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t26.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t26.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t27.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t27.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t28.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t28.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t29.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t29.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t30.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t30.jpg" alt="" class="w-100">
                </a>
              </div>
              
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t31.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t31.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t32.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t32.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t33.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t33.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t34.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t34.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t35.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t35.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t36.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t36.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t37.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t37.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t38.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t38.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t39.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t39.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t40.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t40.jpg" alt="" class="w-100">
                </a>
              </div>
              
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t41.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t41.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t42.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t42.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t43.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t43.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t44.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t44.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t45.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t45.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t46.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t46.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t47.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t47.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t48.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t48.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t49.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t49.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t50.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t50.jpg" alt="" class="w-100">
                </a>
              </div>
              
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t51.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t51.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t52.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t52.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t53.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t53.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t54.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t54.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t55.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t55.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t56.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t56.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t57.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t57.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t58.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t58.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t59.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t59.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t60.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t60.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t61.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t61.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t62.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t62.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t63.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t63.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t64.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t64.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t65.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t65.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t66.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t66.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t67.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t67.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t68.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t68.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t69.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t69.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t70.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t70.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t71.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t71.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t72.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t72.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t73.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t73.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t74.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t74.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t75.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t75.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t76.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t76.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t77.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t77.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t78.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t78.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t79.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t79.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t80.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t80.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t81.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t81.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t82.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t82.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t83.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t83.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t84.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t84.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t85.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t85.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t86.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t86.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/t87.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/t87.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/c1.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/c1.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/c2.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/c2.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/c3.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/c3.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/c4.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/c4.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/c5.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/c5.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/c6.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/c6.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/c7.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/c7.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/c8.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/c8.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/c9.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/c9.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/c10.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/c10.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/c11.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/c11.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/c12.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/c12.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/c13.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/c13.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/c14.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/c14.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/c15.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/c15.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/c16.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/c16.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/c17.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/c17.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p1.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p1.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p2.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p2.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p3.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p3.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p4.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p4.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p5.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p5.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p6.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p6.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p7.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p7.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p8.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p9.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p9.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p10.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p10.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p11.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p11.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p12.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p12.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p13.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p13.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p14.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p14.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p15.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p15.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p16.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p16.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p17.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p17.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p18.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p1.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p19.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p19.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p20.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p20.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p21.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p21.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p22.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p22.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p23.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p23.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p24.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p24.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p25.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p25.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p26.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p26.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p27.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p27.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p28.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p2.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p29.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p29.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p30.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p30.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p31.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p31.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p32.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p32.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p33.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p33.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p34.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p34.jpg" alt="" class="w-100">
                </a>
              </div>
              <div class="col-6 col-lg-4 mb-3">
                <a href="assets/newimg/p35.jpg" data-lightbox="roadtrip">
                  <img src="assets/newimg/p35.jpg" alt="" class="w-100">
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<?php include 'footer.php'; ?>

<script>
$(document).ready(function(){
    $('.modal').each(function(){
            var src = $(this).find('iframe').attr('src');

        $(this).on('click', function(){

            $(this).find('iframe').attr('src', '');
            $(this).find('iframe').attr('src', src);

        });
    });
    $( "#buttonshow" ).click(function() {
      $(this).addClass("d-none")
      $("#dontshow").removeClass("d-none")
    });
    
});
</script>
