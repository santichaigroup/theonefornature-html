<?php include 'header.php'; ?>
  <section>
    <?php include 'nav.php'; ?>
  </section>

  <div class="nt__commitment d-flex justify-content-center align-items-center flex-column">
    <div class="wrapper-2">
      <div class="container-fluid">
        <div class="text-center">
          <h1 class="pt-0 trc">
            How would you help?
          </h1>
        </div>
        <div class="row">
          <div class="col-12 text-center">
            <h6 class="nt__com--title">MOUNTAIN PROTECTOR</h6>
          </div>
          <div class="col-xl-4">
            <div class="commitment-bg active" id="pic_01" style="background-image: url(./assets/img/com-3-1.png);"></div>
            <div class="d-flex justify-content-center">
              <label class="con">
                <input onclick="button_1()" type="radio" name="rr" checked="checked">
                <span class="checkmark"></span>
              </label>
            </div>
          </div>
          <div class="col-xl-4">
            <div class="commitment-bg" id="pic_02" style="background-image: url(./assets/img/com-3-2.png);"></div>
            <div class="d-flex justify-content-center">
              <label class="con">
                <input onclick="button_2()" type="radio" name="rr">
                <span class="checkmark"></span>
              </label>
            </div>
          </div>
          <div class="col-xl-4">
            <div class="commitment-bg" id="pic_03" style="background-image: url(./assets/img/com-3-3.png);"></div>
            <div class="d-flex justify-content-center">
              <label onclick="button_3()" class="con">
                <input type="radio" name="rr">
                <span class="checkmark"></span>
              </label>
            </div>
          </div>
        </div>
      </div>
      <div class="container py-5">
        <div class="d-flex justify-content-center">
          <a href="commitment2.php" class="btn btn-outline-dark btn-lg bpn mx-3 mx-xl-5 px-xl-5">BACK</a>
          <a href="commitment4.php" class="btn btn-primary btn-lg bpn mx-3 mx-xl-5 px-xl-5">NEXT</a>
        </div>
      </div>
    </div>
  </div>

  <script>
  var pic01 = document.querySelector("#pic_01");
  var pic02 = document.querySelector("#pic_02");
  var pic03 = document.querySelector("#pic_03");
  function button_1() {
    if(!pic01.classList.contains("active")) {
      pic01.classList.add("active")
      pic02.classList.remove('active');
      pic03.classList.remove('active');
    } else {
      pic02.classList.remove('active');
      pic03.classList.remove('active');
    }
  }
  function button_2() {
    if(!pic02.classList.contains("active")) {
      pic02.classList.add("active")
      pic01.classList.remove('active');
      pic03.classList.remove('active');
    } else {
      pic01.classList.remove('active');
      pic03.classList.remove('active');
    }
  }
  function button_3() {
    if(!pic03.classList.contains("active")) {
      pic03.classList.add("active")
      pic01.classList.remove('active');
      pic02.classList.remove('active');
    } else {
      pic01.classList.remove('active');
      pic02.classList.remove('active');
    }
  }
  </script>

<?php include 'footer.php'; ?>