<?php include 'header.php'; ?>
  <section>
    <?php include 'nav.php'; ?>
  </section>
  
  <div class="nt__inner">
    <div class="nt__inner-bg" style="background-image: url(./assets/img/in-bg-7.png);">
      <h1 class="text-white">
        Surat Thani
      </h1>
    </div>

    <div class="container mt-small">
      <div class="row">
        <div class="col-lg-5">
          <img src="./assets/img/ex_07.png" alt="Inner" class="mb-3">
        </div>
        <div class="col-lg-7">
          <h2 class="text-uppercase gray"><img src="./assets/img/true.png" alt="T" style="vertical-align: baseline;"> Noistar Thai Animal Rescue Foundation</h2>
          <a href="#" class="badge badge-primary p-2">Share</a>
          <p class="my-3 font-weight-ligther" style="font-size: 1.5rem;">They are a veterinarian clinic servicing the community of Koh Tao. We provide care and treatment to sick and injured stray animals and also to the loved pet community. We pride ourselves on the care we provide to all animals including strays and un-homed animals and no animal in need is turned away.</p> 
          <p class="mb-3"><b>Your role:</b> be the one.. Who help them take care for their animal. If you’re veterinarians or graduates of vet school, you will need at least 2 years experience to qualify for.</p>
          <p class="mb-0"><b>Location:</b> Koh Tao, Surat Thani</p>
          <p class="mb-0">Learn more</p>
          <a href="http://www.kohtaoanimalclinic.org/">http://www.kohtaoanimalclinic.org/</a>
        </div>
      </div>
      <div class="gallery my-3">
        <figure class="gallery__item t2 gallery__item--1">
          <img src="./assets/img/7i1.jpg" class="gallery__img" alt="Image 1">
        </figure>
        <figure class="gallery__item t2 gallery__item--2">
          <img src="./assets/img/7i2.jpg" class="gallery__img" alt="Image 2">
        </figure>
        <figure class="gallery__item t2 gallery__item--3">
          <img src="./assets/img/7i3.jpg" class="gallery__img" alt="Image 2">
        </figure>
      </div>
      <div class="google-maps">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d303780.28405069106!2d100.49000783766152!3d13.612406933369396!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311d6032280d61f3%3A0x10100b25de24820!2z4LiB4Lij4Li44LiH4LmA4LiX4Lie4Lih4Lir4Liy4LiZ4LiE4Lij!5e0!3m2!1sth!2sth!4v1574347867769!5m2!1sth!2sth" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
      </div>
      
      <?php $i = 7; ?>
      <?php include 'inner-sidebar.php'; ?>
    </div>
  </div>

<?php include 'footer.php'; ?>