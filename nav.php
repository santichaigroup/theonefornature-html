<?php 
if($i == 1) { 
  $event = '#event';
  $explore = '#explore';
  $vdo = '#vdo';
} else {
  $event = 'index.php#event';
  $explore = 'index.php#explore';
  $vdo = 'index.php#vdo';
}?>

<div class="nt__nav">
  <div class="container px-lg-0">
    <nav class="navbar navbar-expand-lg navbar-dark font-weight-lighter pb-0 p-lg-0 d-flex">
      <a class="navbar-brand order-2 order-lg-1" href="index.php">
        <img src="assets/img/logo.png" alt="The One for Nature" style="max-height: 45px;">
      </a>
      <button class="navbar-toggler order-1 order-lg-2 border-0" type="button" onclick="myFunction()">
        <img id="bar_image" class="bar" src="assets/img/bar.png" alt="Bar" width="32" height="32">
      </button>
    
      <div class="collapse navbar-collapse order-4" id="navbarSupportedContent" style="max-height: 290px;">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="commitment1.php">Commitment</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo $event ?>">Event</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo $explore ?>">Explore</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo $vdo ?>">VDO</a>
          </li>
        </ul>
        <span class="navbar-text">
          <img class="d-none d-lg-block" src="assets/img/logo_amazing.png" alt="Logo Amazing" style="max-height:32px;">
        </span>
      </div>
      <img class="d-lg-none order-3" src="assets/img/logo_amazing.png" alt="Logo Amazing" style="max-height:32px;">
    </nav>
  </div>
</div>

<script>
function myFunction() {
   var element = document.querySelector("#navbarSupportedContent");
   var image = document.querySelector("#bar_image");
   if(!element.classList.contains("show")) {
    element.classList.toggle("show")
    image.src = "assets/img/x.png";
   } else {
    element.classList.toggle("show")
    image.src = "assets/img/bar.png";
   }
}
</script>