<footer class="nt__footer">
    <small>© 2019 The One For Nature. All rights reserved.</small>
  </footer>

  <script src="node_modules/jquery/dist/jquery.min.js"></script>
  <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
  <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="node_modules/@fortawesome/fontawesome-free/js/all.min.js"></script>
  <script src="assets/lightbox/js/lightbox.min.js"></script>
</body>
</html>