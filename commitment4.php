<?php include 'header.php'; ?>
  <section>
    <?php include 'nav.php'; ?>
  </section>

  <div class="nt__commitment delete d-flex justify-content-center align-items-center flex-column">
    <div class="container">
      <div class="text-center text-size-responsive">
        <h1 class="pt-0">
          SHARE YOUR THOUGHT <br>
          TO SAVE NATURE WITH HASHTAG
        </h1>
        <h2 class="small-font">"#TheOneForNature" and "#AmazingThailand" <br>
            on Facebook and set your post as public.</h2>
      </div>
      <div class="row py-3 justify-content-center">
        <?php $url = 'http://www.theonefornature.com'; ?>
        <div class="col-lg-10">
          <div id="carouselCom" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <a class="facebook-share-button" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode($url); ?>" target="_blank">
                  <img src="./assets/img/com-4-1.png" class="d-block w-100" alt="..." title="facebook_link" alt="facebook_link">
                </a>
              </div>
              <div class="carousel-item">
                <a class="facebook-share-button" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode($url); ?>" target="_blank">
                  <img src="./assets/img/com-4-2.png" class="d-block w-100" alt="..." title="facebook_link" alt="facebook_link">
                </a>
              </div>
              <div class="carousel-item">
                <a class="facebook-share-button" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode($url); ?>" target="_blank">
                  <img src="./assets/img/com-4-3.png" class="d-block w-100" alt="..." title="facebook_link" alt="facebook_link">
                </a>
              </div>
            </div>
            <a class="carousel-control-prev" href="#carouselCom" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselCom" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
      <div class="container py-5">
        <div class="d-flex justify-content-center">
          <a href="commitment3.php" class="btn btn-outline-dark btn-lg bpn mx-3 mx-xl-5 px-xl-5">BACK</a>
          <a href="commitment5.php" class="btn btn-primary btn-lg bpn mx-3 mx-xl-5 px-xl-5">SHARE <i class="fab fa-facebook-f"></i></a>
        </div>
      </div>
    </div>
  </div>

<?php include 'footer.php'; ?>